import org.opencv.core.Core
import org.opencv.core.Core.minMaxLoc
import org.opencv.core.Mat
import java.awt.Rectangle
import java.awt.Robot
import java.io.File
import org.opencv.imgproc.Imgproc.*
import java.awt.event.KeyEvent
import java.awt.image.BufferedImage
import org.nield.kotlinstatistics.*

val samples = listOf(
        Sample(6,75,ObstacleType.Low,ActionType.Jump,true),
        Sample(6,75,ObstacleType.High,ActionType.Duck,true),
        Sample(7,80,ObstacleType.Low,ActionType.Jump,true),
        Sample(7,80,ObstacleType.High,ActionType.Duck,true),
        Sample(8,84,ObstacleType.Low,ActionType.Jump,true),
        Sample(8,84,ObstacleType.High,ActionType.Duck,true),
        Sample(6,80,ObstacleType.None,ActionType.Idle,true)
)

val nbc = samples.toNaiveBayesClassifier(featuresSelector = {
    setOf(it.speed,it.distance,it.obstacleType,it.success)
},categorySelector = {it.actionType})


fun main(args: Array<String>) {

    System.loadLibrary(Core.NATIVE_LIBRARY_NAME)

    val dinoImage = loadImage(File("dino.png"))

    val robot = Robot()

    val playArea = Rectangle(0, 0, 1920, 1080)

    var isPlayAreaFound = false

    while (!isPlayAreaFound) {
        var time = System.currentTimeMillis()
        val image = robot.createScreenCapture(playArea)
        println("Capturing screenshot ${System.currentTimeMillis() - time}")

        val result = Mat()
        time = System.currentTimeMillis()
        val screenImage = image.toOpenCvMat()
        println("Converting image into opencv ${System.currentTimeMillis() - time}")

        time = System.currentTimeMillis()
        matchTemplate(screenImage, dinoImage, result, TM_CCOEFF_NORMED)
        println("Finding dyno ${System.currentTimeMillis() - time}")


        time = System.currentTimeMillis()
        val res = minMaxLoc(result)
        println("Finding coordinates ${System.currentTimeMillis() - time}")

        if (res.maxVal > 0.995) {
            playArea.setLocation(res.maxLoc.x.toInt() + 70, res.maxLoc.y.toInt() - 50)
            playArea.setSize(400, 120)

            println("[${playArea.x} , ${playArea.y}]")

            isPlayAreaFound = true
        }

    }

    var started = false
    var canGoGameOver = false

    var oldImage: BufferedImage? = null

    var gameOverFrames = 0

    var isDucking = false
    var duckingFrames = 0

    var isJumping = false
    var jumpingFrames = 0

    var startTime:Long=0


    while (true) {
        //sapendo dov'è la playArea cerco gli ostacoli davanti al dinosauro
        val image = robot.createScreenCapture(playArea)

        if (isDucking) {
            duckingFrames++

            if (duckingFrames > 15) {
                isDucking = false
                duckingFrames = 0
                robot.keyRelease(KeyEvent.VK_DOWN)
            }
        }

        if (isJumping) {
            jumpingFrames++

            if (jumpingFrames > 15) {
                isJumping = false
                jumpingFrames = 0
                robot.keyRelease(KeyEvent.VK_SPACE)
            }
        }


        if (started) {

            val gameTime=System.currentTimeMillis()-startTime

            val action=checkHorizontally(image,gameTime)


            if (action==ActionType.Jump) {
           //     println("JUMP!")
                //L'ostacolo è in basso, lo salto
                robot.keyPress(KeyEvent.VK_SPACE)
                //robot.keyRelease(KeyEvent.VK_SPACE)
                canGoGameOver=true
                isJumping = true
            }else if (action==ActionType.Duck) {
             //   println("DUCK!")
                //L'ostacolo è in alto, mi abbasso
                robot.keyPress(KeyEvent.VK_DOWN)
                // println("DUCKING!!!!")
                isDucking = true
            }

            if (canGoGameOver && inGameOver(image, oldImage, 40, 40)) {
                gameOverFrames++
                println("Maybe gameOver")
                if (gameOverFrames > 2) {
                    canGoGameOver = false
                    started = false
                    robot.keyPress(KeyEvent.VK_F5)
                    robot.keyRelease(KeyEvent.VK_F5)
                    println("GameOver!")
                }
            } else {
                gameOverFrames = 0
            }
        } else {
            //controllo che la partita sia iniziata
            if (isStarted(image, oldImage, 40, 40)) {
                started = true
                println("Started!!")
                startTime = System.currentTimeMillis()
            } else {
                //provo a far partire il gioco
                if (!started)
                    robot.keyPress(KeyEvent.VK_SPACE)
            }
        }
        oldImage = image
    }

}

const val checkWidth = 399
const val LowObstacleCheckY=75
const val HighObstacleCheckY=45
var secondLowObstacleFound=false

var actualVelocity=5
var lastGameTimeVelocityCheck=0L
var measuringVelocity=false
var firstMeasuringPoint=0

fun checkHorizontally( currentFrame: BufferedImage,gameTime:Long):ActionType {

    var lowObstacleDistance = Int.MAX_VALUE
    var highObstacleDistance = Int.MAX_VALUE
    secondLowObstacleFound=false

    //controllo per gli ostacoli bassi e alti
    for (x in 35 until checkWidth) {
        //controllo se ci sono ostacoli ad altezza "Low Obstacles"
        if (currentFrame.hasColorDelta(x, x - 1, LowObstacleCheckY)) {
            if(lowObstacleDistance==Int.MAX_VALUE) {

                //ho trovato un "Low Obstacle", mi salvo le sue coordinate
                lowObstacleDistance = x
                //ora mi aspetto di trovare la fine dell'ostacolo
            }else{
                //ho trovato la fine dell'ostacolo, oppure l'inizio di un altro ostacolo,
                //controllo quant'è lontano dal primo che ho trovato
                val difference=x-lowObstacleDistance
                if(difference>70 && difference < 220+(actualVelocity*3)){
                    //la differenza di colore è abbastanza lontana dall'inizio del primo ostacolo, la considero come secondo ostacolo
                    println("Distance ${x-lowObstacleDistance} to good, is second Obstacle")
                    secondLowObstacleFound=true
                //    val output=File("testImage.jpg")
                //    ImageIO.write(currentFrame,"jpg",output)
                    break
                }
            }

        }

        //controllo se ci sono ostacoli ad altezza "High Obstacles"
        if ( currentFrame.hasColorDelta(x, x - 1, HighObstacleCheckY)) {
            //ho trovato un "High Obstacle", mi salvo le sue coordinate ed esco dl ciclo
            highObstacleDistance = x
            break
        }
    }

    if(gameTime-lastGameTimeVelocityCheck>500 && lowObstacleDistance!= Int.MAX_VALUE){
    //    println("Checking Velocity!")
        if(!measuringVelocity) {
            firstMeasuringPoint = lowObstacleDistance
            measuringVelocity=true
        }else{
            val calculated=firstMeasuringPoint-lowObstacleDistance
            if(calculated>actualVelocity) {
                actualVelocity = calculated
                println("Actual velocity:$actualVelocity")
            }
            measuringVelocity=false
            lastGameTimeVelocityCheck=gameTime

        }
    }

    var currentObstacle=ObstacleType.None
    var distance=-1

    if(lowObstacleDistance!= Int.MAX_VALUE) {
        currentObstacle=if(secondLowObstacleFound) ObstacleType.LowWithSecond else ObstacleType.Low
        distance=lowObstacleDistance
    }else if(highObstacleDistance !=Int.MAX_VALUE) {
        currentObstacle=ObstacleType.High
        distance=highObstacleDistance
    }

    val actionByNbc=nbc.predict(actualVelocity,distance,currentObstacle,true)
    if(actionByNbc!=ActionType.Idle)
        println("with velocity $actualVelocity, distance $distance, obstacle $currentObstacle, NBC predicted $actionByNbc")

    return actionByNbc!!
}

fun BufferedImage.hasColorDelta(x1: Int, x2: Int, y1: Int, y2: Int = y1): Boolean =
        this.getRGB(x2,y2)- this.getRGB(x1,y1) >= 394758


fun inGameOver(thisFrame: BufferedImage, oldFrame: BufferedImage?, width: Int, height: Int): Boolean =
     !isTerrainMoving(thisFrame,oldFrame,width,height)

fun isStarted(thisFrame: BufferedImage, oldFrame: BufferedImage?, width: Int, height: Int): Boolean =
     isTerrainMoving(thisFrame,oldFrame,width,height)

fun isTerrainMoving(thisFrame: BufferedImage, oldFrame: BufferedImage?, width: Int, height: Int): Boolean {
    if (oldFrame != null) {
        for (x in 0 until width) {
            for (y in 80 until 80 + height) {
                if (oldFrame.getRGB(x, y) != thisFrame.getRGB(x, y)) {
                    return true
                }
            }
        }
    } else {
        return false
    }
    return false
}


